#!/bin/bash
# this shortcut will open visual studio code in the current directory on a UNIX based operating system (draft)
DIRPATH="$(PWD)"

function test_for_dir() {
  if [ -z "$1" ]; then 
    echo "Invalid directory. Aborting"
    exit 1
  fi
}

# Handle different requests to the script (open, create)
while test $# -gt 0; do 
  case "$1" in 
    -d|--dir)
      shift 
      test_for_dir $@
      # Found path
      DIRPATH="$1"
      shift
      ;;

    *)
      test_for_dir $@
      # Found path
      DIRPATH="$1"
      break
      ;;
  esac
done

if [ -z "$DIRPATH" ]; then 
   DIRPATH=$(PWD)
fi

# Check for workspace file
WORKSPACE_FILE=$(find ${DIRPATH} -regex ".*\.code-workspace" | head -n 1)
if [ -z "${WORKSPACE_FILE}" ]; then 
  open -a "Visual Studio Code" $DIRPATH
else
  open -a "Visual Studio Code" "${WORKSPACE_FILE}"
fi

# open -a "visual studio code" $DIRPATH
